# Docker MySQL Example - Application

---

Stack: Docker, Debian, MySQL.
This is an example application that can setup multiple dev environments quickly with docker.

## Mac OS X: Local Development

1. Start with a Mac
2. Install latest VirtualBox
3. Install Docker and Boot2Docker, that comes with the install: https://docs.docker.com/installation/mac
4. THIS PROJECT MUST BE CHECKED OUT HERE: /Users/Sites/docker-mysql-example
5. Open up a terminal, and cd /Users/Sites/docker-mysql-example
6. Run: boot2docker init
7. Run: boot2docker up
8. NOTE: Don't know the ip, just type: boot2docker ip
9. Open: /Users/{user}/.bash_profile, and paste
9. export DOCKER_HOST=tcp://{generated_ip}:2376
9. export DOCKER_CERT_PATH=/Users/{user}/.boot2docker/certs/boot2docker-vm
9. export DOCKER_TLS_VERIFY=1
10. Restart terminal
11. cd back to this root folder
12. Get Docker Compose
13. NOTE: Always check here: https://github.com/docker/fig/releases, for the latest release:
```
curl -L https://github.com/docker/fig/releases/download/1.1.0-rc2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose; chmod +x /usr/local/bin/docker-compose
```

## Environment: Docker Compose Files

1. LOCAL Up: docker-compose up -d

## MySQL: Local Connection

1. MySQL Host: 127.0.0.1
2. Username: root
3. Password: root
4. Database: {database_name}
5. Port: 3306
6. SSH Host: {generated_ip}
7. SSH User: docker
8. SSH Password: tcuser
9. SSH Port: 22